//
//  GameViewController.m
//  ProtoRope
//
//  Created by Marc FERRY on 19/08/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "GameViewController.h"
#import "PRJumpScene.h"

@implementation GameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = NO;
    skView.showsNodeCount = NO;
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    skView.ignoresSiblingOrder = YES;

    // Create and configure the scene.
    SKScene *gameScene  = [[PRJumpScene alloc] initWithSize:self.view.frame.size];
    gameScene.scaleMode = SKSceneScaleModeAspectFill;

    // Present the scene.
    [skView presentScene:gameScene];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    else
    {
        return UIInterfaceOrientationMaskAll;
    }
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
