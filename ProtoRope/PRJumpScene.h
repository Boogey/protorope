//
//  MyGameScene.h
//  ProtoRope
//
//  Created by Pierre Ferry on 21/08/14.
//
//

#ifndef ProtoRope_MyGameScene_h
#define ProtoRope_MyGameScene_h

#import <SpriteKit/SpriteKit.h>
#import "PRPlayerNode.h"

typedef NS_ENUM(uint32_t, CollisionType) {
    playerCategory      = 0x1 << 0,
    enemyCategory       = 0x1 << 1,
    groundCategory      = 0x1 << 2,
};

@interface PRJumpScene : SKScene<SKPhysicsContactDelegate>

@end

#endif
