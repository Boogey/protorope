//
//  JumpBall.m
//  ProtoRope
//
//  Created by Pierre Ferry on 21/08/14.
//
//

#import "PRPlayerNode.h"

@interface PRPlayerNode ()

@property (strong, nonatomic) NSArray *playerJumpFrames;

@end

@implementation PRPlayerNode

static CGFloat jumpSpeed = 600.00;

/**************************************************************************************************/
#pragma mark - Init methods

- (id)initWithTexture:(SKTexture *)texture
{
    if (self = [super initWithTexture:texture])
    {
        self.isOnGround = NO;
        self.jumpCount = 0;

        // Load jump animation sprites
        NSMutableArray *jumpFrames = [NSMutableArray array];
        SKTextureAtlas *jumpAnimatedAtlas = [SKTextureAtlas atlasNamed:@"player"];

        for (int i = 0; i <= 3; i++)
        {
            NSString *textureName = [NSString stringWithFormat:@"jump%d", i];
            SKTexture *temp = [jumpAnimatedAtlas textureNamed:textureName];
            [jumpFrames addObject:temp];
        }
        self.playerJumpFrames = jumpFrames;
    }
    return self;
}


/**************************************************************************************************/
#pragma mark -

- (void)isFalling
{
    [self animateFall];
}

- (void)didJump:(CGPoint)direction
{
    self.physicsBody.dynamic = YES;
    NSLog(@"jumpdirection x: %f, y: %f", direction.x , direction.y);
    self.physicsBody.velocity = CGVectorMake(direction.x * jumpSpeed, labs(direction.y * jumpSpeed));
    self.isOnGround = NO;
    self.isOnPlatform = NO;
    self.jumpCount++;
    [self animateJump];
}

- (void)didTouchGround
{
    self.isOnGround = YES;
    self.jumpCount = 0;
    [self animateTouchGround];
}

/**************************************************************************************************/
#pragma mark - Animation methods

- (void)animateTouchGround
{
    [self setTexture:[self.playerJumpFrames objectAtIndex:0]];
}

- (void)animateJump
{
    [self runAction:[SKAction repeatActionForever:
                     [SKAction animateWithTextures:self.playerJumpFrames
                                      timePerFrame:0.1f
                                            resize:NO
                                           restore:YES]]
            withKey:@"jump"];
}

- (void)animateFall
{
    [self removeAllActions];
    [self setTexture:[[SKTextureAtlas atlasNamed:@"player"] textureNamed:@"falldown0"]];
}

@end