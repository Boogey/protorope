//
//  AppDelegate.h
//  ProtoRope
//
//  Created by Marc FERRY on 19/08/14.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

