//
//  MyGameScene.m
//  ProtoRope
//
//  Created by Pierre Ferry on 21/08/14.
//
//

#import "PRJumpScene.h"


@interface PRJumpScene ()

@property BOOL                              contentCreated;
@property (strong, nonatomic) PRPlayerNode  *playerNode;
@property (strong, nonatomic) SKSpriteNode  *platformNode;
@property (strong, nonatomic) SKSpriteNode  *groundNode;
@property (strong, nonatomic) SKSpriteNode  *backgroundNode;
@property (strong, nonatomic) SKNode        *WorldNode;
@property (nonatomic) int                   screenCount;
@end

@implementation PRJumpScene


/**************************************************************************************************/
#pragma mark - Init methods

- (id)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size])
    {
        self.physicsWorld.contactDelegate = self;
    }
    return self;
}

/**************************************************************************************************/
#pragma mark - SKScene methods

- (void)didMoveToView:(SKView *)view
{
    if (!self.contentCreated)
    {
        [self createSceneContents];
        self.contentCreated = YES;
    }
}

- (void)createSceneContents
{
    self.backgroundNode = [[SKSpriteNode alloc] initWithTexture:[SKTexture textureWithImageNamed:@"background.jpg"]];
    self.backgroundNode.name = @"background";
    self.backgroundNode.size = CGSizeMake(self.size.width * 2, self.size.height* 2);
    self.backgroundNode.physicsBody.dynamic = NO;
    self.backgroundNode.zPosition = -99;
    [self addChild:self.backgroundNode];
    
    self.scaleMode = SKSceneScaleModeAspectFit;
    self.anchorPoint = CGPointMake (0.5, 0.5);
    
    //Create the world
    SKNode *myWorld = [SKNode node];
    myWorld.name = @"World";
    myWorld.position = CGPointMake(CGRectGetMidX(self.frame) / 2,
                                   0);
    [self addChild:myWorld];
    self.WorldNode = myWorld;

    //Create a camera
    SKNode *camera = [SKNode node];
    camera.name = @"camera";
    camera.position = CGPointMake(CGRectGetMidX(self.frame) / 2,
                                  self.frame.size.height / 2);
    [myWorld addChild:camera];
    
    // Create ground
    SKSpriteNode *ground = [[SKSpriteNode alloc] initWithColor:[SKColor clearColor]
                                                          size:CGSizeMake(self.size.width * 2,
                                                                          self.size.height * 0.2)];
    ground.name = @"ground";
    ground.position = CGPointMake(CGRectGetMidX(myWorld.frame),
                                  -ground.size.height);
    ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:ground.size];
    ground.physicsBody.dynamic = NO;
    self.groundNode = ground;
    
    // Create player
    self.playerNode = [self newPlayer];
    self.playerNode.name = @"player";
    self.playerNode.position = CGPointMake(CGRectGetMidX(self.frame),
                                           CGRectGetMidY(self.frame) + 20);
    
    // Create enemy
    self.platformNode = [self newPlatform];
    
    // Add colision conditions
    self.playerNode.physicsBody.categoryBitMask = playerCategory;
    self.playerNode.physicsBody.collisionBitMask = groundCategory | enemyCategory;
    self.playerNode.physicsBody.contactTestBitMask = groundCategory | enemyCategory;
    ground.physicsBody.categoryBitMask = groundCategory;

    self.screenCount = 1;
    [myWorld addChild:ground];
    [myWorld addChild:self.playerNode];
    
}

- (void)didSimulatePhysics
{
}

- (void)didFinishUpdate
{
    [self centerOnNode:self.playerNode];
    self.backgroundNode.position = CGPointMake(-self.playerNode.position.x, -self.playerNode.position.y + self.size.height / 2 + self.groundNode.size.height);
    //NSLog(@"position self: %f; worl : %f; camera : %f", self.position.y, self.WorldNode.position.y, [self childNodeWithName:@"camera"].position.y);
    
    if (self.WorldNode.position.y < - self.size.height * self.screenCount / 2)
    {
        [self newPlatform];
        self.screenCount++;
    }
    
    if (self.WorldNode.position.y > 100)
    {
        [self resetGame];
    }
}

- (void)didEvaluateActions
{
    if (self.playerNode.isJumping) {
        self.playerNode.physicsBody.dynamic = YES;
        self.playerNode.stick.enabled = NO;
        self.playerNode.isJumping = NO;
        self.playerNode.isOnPlatform = NO;
    }
}

- (void)update:(CFTimeInterval)currentTime
{
    if ((self.playerNode.physicsBody.velocity.dy < 0) && (!self.playerNode.isOnGround))
    {
        [self.playerNode isFalling];
    }
}

- (void)resetGame
{
    SKNode * platforms;
    while ((platforms = [self.WorldNode childNodeWithName:@"enemy"]) != nil)
    {
        [platforms removeAllActions];
        [platforms removeFromParent];
    }
    
    self.playerNode.position= CGPointMake(self.groundNode.position.x, self.groundNode.position.y + self.groundNode.size.height);
    self.screenCount = 0;
}

/**************************************************************************************************/
#pragma mark - Touch events

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGFloat x, y, abs;
    CGPoint direction;

    if (self.playerNode == nil)
    {
        NSLog(@"Did not find player /n");
    }
    else
    {
        // Change player orientation
        CGPoint location = [[touches anyObject] locationInNode:self];
        CGFloat multiplierForDirection = location.x <= CGRectGetMidX(self.frame) ? 1 : -1;
        location = [[touches anyObject] locationInNode:self.playerNode];
        x = /*self.playerNode.position.x*/ - location.x * self.playerNode.xScale / fabs(self.playerNode.xScale);
        y = /*self.playerNode.position.y*/ - location.y;
        abs = sqrt( x * x + y * y );
        direction =  CGPointMake(x / abs, y / abs);
        self.playerNode.xScale = fabs(self.playerNode.xScale) * multiplierForDirection;
        if (self.playerNode.jumpCount < 2 || self.playerNode.isOnGround)
        {
            [self.playerNode didJump:direction];
        }
        self.playerNode.isJumping=TRUE;
        
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
}

/**************************************************************************************************/
#pragma mark - Physics

- (void)didBeginContact:(SKPhysicsContact *)contact
{
    SKPhysicsBody *firstBody, *secondBody;
    SKSpriteNode *contactNode; 
    
    firstBody = (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) ? contact.bodyA : contact.bodyB;
    secondBody = (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) ? contact.bodyB : contact.bodyA;
    
    if ((secondBody.categoryBitMask & groundCategory) != 0)
    {
        self.playerNode.physicsBody.velocity = CGVectorMake(0, 0);
        [self.playerNode removeAllActions];
        [self.playerNode didTouchGround];
    }
    else if ((secondBody.categoryBitMask & enemyCategory) != 0)
    {
        contactNode = (SKSpriteNode *) secondBody.node;
        //NSLog(@"test : %f , %f",self.playerNode.position.y,contactNode.position.y);

        if  ((self.playerNode.position.y > (contactNode.position.y))
             && (!self.playerNode.isOnPlatform)
             && (labs(self.playerNode.position.x - contactNode.position.x) < contactNode.size.width * 0.9 / 2))
        {
            self.playerNode.isOnPlatform = YES;
            self.playerNode.stick = [SKConstraint positionX:[SKRange rangeWithConstantValue:self.playerNode.position.x - contactNode.position.x]
                                                          Y:[SKRange rangeWithConstantValue:contactNode.size.height / 2 + self.playerNode.size.height / 2]];
            NSArray * constraints = [NSArray arrayWithObject:self.playerNode.stick];
            self.playerNode.constraints = constraints;
            self.playerNode.stick.enabled = YES;
            self.playerNode.stick.referenceNode = contactNode;
            [self.playerNode removeAllActions];
            [self.playerNode didTouchGround];
        }
    }
}


/**************************************************************************************************/
#pragma mark - Node creation

- (PRPlayerNode *)newPlayer
{
    PRPlayerNode *player = [[PRPlayerNode alloc] initWithTexture:[[SKTextureAtlas atlasNamed:@"player"] textureNamed:@"jump0"]];
    
    player.size = CGSizeMake(player.size.width * 0.8,
                             player.size.height * 0.8);
    player.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:player.size.height / 2];
    player.physicsBody.dynamic = YES;
    player.physicsBody.mass = 0.0;
    player.physicsBody.restitution = 0.0;
    player.isJumping = NO;
    return player;
}

- (SKSpriteNode *)newPlatform
{
    SKSpriteNode * platform;
    platform = [[SKSpriteNode alloc] initWithColor:[SKColor redColor]
                                                       size:CGSizeMake(self.playerNode.size.width * 2,
                                                                       self.playerNode.size.width / 2)];
    platform.name = @"enemy";
    platform.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:platform.size];
    platform.physicsBody.dynamic = NO;
    platform.physicsBody.restitution = 0.0;
    platform.position = CGPointMake(self.playerNode.position.x,
                                             self.playerNode.position.y + self.frame.size.height / 5);
    
    // Define enemy path
    CGFloat circleRadius = self.size.height / 3;
    UIBezierPath *circle = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0 - circleRadius / 2,
                                                                              0,
                                                                              circleRadius,
                                                                              circleRadius)
                                                      cornerRadius:100];
    SKAction *followCircle = [SKAction followPath:circle.CGPath
                                         asOffset:YES
                                     orientToPath:NO
                                         duration:3.0];
    [platform runAction:[SKAction repeatActionForever:followCircle]];
    platform.physicsBody.categoryBitMask = enemyCategory;
    [self.WorldNode addChild:platform];
    return platform;
}


/**************************************************************************************************/
#pragma mark - Node utilities

- (void)centerOnNode:(SKNode *)node
{
    CGPoint cameraPositionInScene = [node.scene convertPoint:node.position fromNode:node.parent];
    node.parent.position = CGPointMake(node.parent.position.x - cameraPositionInScene.x,
                                       node.parent.position.y - cameraPositionInScene.y);
}


@end
