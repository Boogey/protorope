//
//  SKSpriteNode_JumpBall.h
//  ProtoRope
//
//  Created by Pierre Ferry on 21/08/14.
//
//

#import <SpriteKit/SpriteKit.h>

@interface PRPlayerNode : SKSpriteNode


@property (nonatomic) BOOL  isOnGround;
@property (nonatomic) BOOL  isOnPlatform;
@property (nonatomic) BOOL  isTouching;
@property (nonatomic) BOOL   isJumping;
@property (nonatomic) int   jumpCount;
@property (nonatomic) SKConstraint   *stick;


- (void)isFalling;
- (void)didJump:(CGPoint)direction;
- (void)didTouchGround;

@end

